﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Shop.Dto;
using Shop.Enums;
using Shop.Models;

namespace Shop
{
	public class ProductService
	{
		private readonly ShopContext _dbContext;

		private IList<Product> Products => _dbContext.Products.Include(p => p.Category).ToList();

		public ProductService(ShopContext dbContext)
		{
			_dbContext = dbContext;
		}

		/// <summary>
		/// Сортирует товары по цене и возвращает отсортированный список
		/// </summary>
		/// <param name="sortOrder">Порядок сортировки</param>
		public IEnumerable<Product> SortByPrice(SortOrder sortOrder)
		{
			if (sortOrder == SortOrder.Ascending)
				return Products.OrderBy(p => p.Price);
			else
				return Products.OrderByDescending(p => p.Price);
		}

		/// <summary>
		/// Возвращает товары, название которых начинается на <see cref="name"/>
		/// </summary>
		/// <param name="name">Фильтр - строка, с которой начинается название товара</param>
		public IEnumerable<Product> FilterByNameStart(string name)
		{
			return Products.Where(p => p.Name == name);
		}

		/// <summary>
		/// Группирует товары по производителю
		/// </summary>
		public IDictionary<string, List<Product>> GroupByVendor()
		{
			return Products.GroupBy(p => p.Vendor).ToDictionary(p => p.Key, p => p.ToList());
		}

		/// <summary>
		/// Возвращает список самых дорогих товаров (самые дорогие - товары с наибольшей ценой среди всех товаров)
		/// </summary>
		public IEnumerable<Product> GetTheMostExpensiveProducts()
		{
			return Products.Where(p => p.Price == Products.Max(p => p.Price));
		}

		/// <summary>
		/// Возвращает список самых дешевых товаров (самые дешевые - товары с наименьшей ценой среди всех товаров)
		/// </summary>
		public IEnumerable<Product> GetTheCheapestProducts()
		{
			return Products.Where(p => p.Price == Products.Min(p => p.Price));
		}

		/// <summary>
		/// Возвращает среднюю цену среди всех товаров
		/// </summary>
		public decimal GetAverageProductPrice()
		{
			return Products.Average(p => p.Price);
		}

		/// <summary>
		/// Возвращает среднюю цену товаров в указанной категории
		/// </summary>
		/// <param name="categoryId">Идентификатор категории</param>
		public decimal GetAverageProductPriceInCategory(int categoryId)
		{
			return Products.Where(p => p.CategoryId == categoryId).Average(p => p.Price);
		}

		/// <summary>
		/// Возвращает список продуктов с актуальной ценой (после применения скидки)
		/// </summary>
		public IDictionary<Product, decimal> GetProductsWithActualPrice()
		{
			return Products.Select(p => { p.Price -= (p.Price * p.Discount / p.Price); return p; }).ToDictionary(p => p, p => p.Price);
		}

		/// <summary>
		/// Возвращает список продуктов, сгруппированный по производителю, а внутри - по названию категории.
		/// Продукты внутри последней группы отсортированы в порядке убывания цены
		/// </summary>
		public IList<VendorProductsDto> GetGroupedByVendorAndCategoryProducts()
		{
			var c = Products.OrderByDescending(p => p.Price).GroupBy(p => p.Vendor).ToDictionary(p => p.Key, p => p.GroupBy(p => p.Category).ToDictionary(p => p.Key.Name, p => p.ToList()));
			var result = new List<VendorProductsDto>();
			foreach (var product in c)
				result.Add(new VendorProductsDto() {Vendor = product.Key, CategoryProducts = product.Value });

			return result;
		}

		/// <summary>
		/// Обновляет скидку на товары, которые остались на складе в количестве 1 шт,
		/// и возвращает список обновленных товаров
		/// </summary>
		/// <param name="newDiscount">Новый процент скидки</param>
		public IEnumerable<Product> UpdateDiscountIfUnitsInStockEquals1AndGetUpdatedProducts(int newDiscount)
		{
			return Products.Where(p => p.UnitsInStock == 1).Select(x => { x.Discount = newDiscount; return x; });
		}
	}
}
